```yaml
include:
  - '/ci/init/init.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Initialize
#-----------------------------------------------------------------------------------------
🚀:initialize:
  image: node:13.12-slim
  stage: 🖐️various
  extends: .init:files
```