const fs = require('fs')

let what = "<visual-review></visual-review>"
let by = `
<script
  data-project-id="${process.env.CI_PROJECT_ID}"
  data-merge-request-id="${process.env.CI_MERGE_REQUEST_IID}"
  data-mr-url="https://gitlab.com"
  data-project-path="${process.env.CI_PROJECT_PATH}"
  data-require-auth="true"
  id="review-app-toolbar-script"
  src="https://gitlab.com/assets/webpack/visual_review_toolbar.js">
</script>
`
if(process.env.CI_MERGE_REQUEST_IID!=="") {
  fs.readFile(`./public/index.html`, 'utf8', (err,data) => {
    if (err) {
      return console.log(err)
    }
    let result = data.replace(what, by)
  
    fs.writeFile(`./public/index.html`, result, 'utf8', (err) => {
       if (err) return console.log(err)
    })
  })
}

/*
<script
data-project-id="${CI_PROJECT_ID}"
data-merge-request-id="${CI_MERGE_REQUEST_IID}"
data-mr-url="https://gitlab.com"
data-project-path="${CI_PROJECT_PATH}"
data-require-auth="true"
id="review-app-toolbar-script"
src="https://gitlab.com/assets/webpack/visual_review_toolbar.js">
</script>
*/